import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GdkPixbuf, Pango
from collections import OrderedDict
import signal, os, Levenshtein, json

class EmojiKeyboard(Gtk.Window):
    emojidata = json.load(open(os.path.join(os.path.dirname(__file__), "res/emoji.json")), object_pairs_hook=OrderedDict)

    def __init__(self):
        Gtk.Window.__init__(self, title="Emoji Keyboard")

        self.set_border_width(5)
        self.set_default_size(500, 300)
        self.connect("key-press-event", self.on_key_pressed)

        self.vbox = Gtk.VBox()
        self.add(self.vbox)

        self.stack = Gtk.Stack()
        self.vbox.pack_end(self.stack, True, True, 0)

        self.notebook = Gtk.Notebook()
        self.notebook.set_scrollable(True)
        self.stack.add_titled(self.notebook, "categories", "Categories")

        EmojiKeyboard.favorites_page = EmojiContainer()
        self.notebook.append_page(EmojiKeyboard.favorites_page, Gtk.Label("Favorites"))

        for category, emojis in self.emojidata.items():
            page = EmojiContainer(emojis)
            self.notebook.append_page(page, Gtk.Label(category))

        self.notebook.show_all()
        self.notebook.set_current_page(1)

        self.search_results_wrapper = Gtk.ScrolledWindow()
        self.stack.add_titled(self.search_results_wrapper, "search_results", "Search Results")

        self.search_results = EmojiContainer()
        self.search_results_wrapper.add(self.search_results)

        self.searchbox = Gtk.Entry()
        self.searchbox.set_placeholder_text("Search")
        self.searchbox.connect("changed", self.on_search_changed)
        self.searchbox.set_name("searchbox")
        self.vbox.pack_end(self.searchbox, False, False, 5)

        self.srv = False

        css = Gtk.CssProvider()
        css.load_from_path(os.path.join(os.path.dirname(__file__), "res/css/keyboard.css"))
        self.get_style_context().add_provider(css, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    def on_key_pressed(self, widget, event):
        if event.keyval == 65307:  # Esc key
            self.hide()
        else:
            if not self.searchbox.is_focus():
                self.searchbox.grab_focus()

    def on_search_changed(self, user_data):
        if self.searchbox.get_text():
            self.stack.set_visible_child(self.search_results_wrapper)

            self.search_results.clear()

            for emoji in sorted([emoji for category, emojis in self.emojidata.items() for name, emoji in emojis.items() if self.searchbox.get_text().lower() in emoji.get("name").lower() or self.searchbox.get_text().lower() in " ".join(emoji.get("keywords"))], key=lambda emoji: (1 - Levenshtein.ratio(self.searchbox.get_text(), emoji.get("name"))) + (1 - Levenshtein.setratio([self.searchbox.get_text()], emoji.get("keywords")))):
                self.search_results.add_emoji(emoji)
        else:
            self.stack.set_visible_child(self.notebook)

class TrayIcon(Gtk.StatusIcon):

    def __init__(self, window):
        Gtk.StatusIcon.__init__(self)

        self.window = window

        self.set_from_file(os.path.join(os.path.dirname(__file__), "res/icon.png"))
        self.set_has_tooltip(True)
        self.set_tooltip_text("Emoji Keyboard")
        self.set_visible(True)
        self.connect("activate", lambda e: self.window.show())
        self.connect("popup_menu", self.on_secondary_click)

    def on_secondary_click(self, widget, button, time):
        menu = Gtk.Menu()

        favorites_menu = Gtk.Menu()

        favorites_item = Gtk.MenuItem("Favorites")
        favorites_item.set_submenu(favorites_menu)

        menu.append(favorites_item)

        open_item = Gtk.MenuItem("Open")
        open_item.connect("activate", lambda e: self.window.show())
        menu.append(open_item)

        quit_item = Gtk.MenuItem("Quit")
        quit_item.connect("activate", Gtk.main_quit)
        menu.append(quit_item)

        menu.show_all()
        menu.popup(None, None, None, self, button, time)

class EmojiContainer(Gtk.ScrolledWindow):
    def __init__(self, emojis={}):
        super().__init__()

        self.flow = Gtk.FlowBox()
        self.add(self.flow)

        self.flow.set_row_spacing(5)
        self.flow.set_column_spacing(5)
        self.flow.set_valign(Gtk.Align.START)
        self.flow.set_max_children_per_line(50)
        self.flow.set_selection_mode(Gtk.SelectionMode.NONE)

        for name, emoji in emojis.items():
            button = EmojiButton(emoji)
            self.flow.add(button)

    def add_emoji(self, emoji):
        button = EmojiButton(emoji)
        self.flow.add(button)
        button.show()

    def remove_emoji(self, emoji):
        return # TODO: implement this

    def clear(self):
        for child in self.flow.get_children():
            self.flow.remove(child)

class EmojiButton(Gtk.Button):

    def __init__(self, emoji):
        Gtk.Button.__init__(self)

        self.emoji = emoji

        path = os.path.join(os.path.dirname(__file__), "res/emoji/png_64/" + self.emoji.get("unicode") + ".png")
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(path, 24, 24)
        image = Gtk.Image.new_from_pixbuf(pixbuf)

        self.add(image)
        image.show()

        self.connect("clicked", self.on_clicked)

    def on_clicked(self, button):
    	win = EmojiPopup(self.get_toplevel(), self.emoji)
    	win.show_all()

class EmojiPopup(Gtk.Dialog):

    def __init__(self, parent, emoji):
        Gtk.Dialog.__init__(self, emoji.get("name").title(), parent, 0, flags = Gtk.DialogFlags.MODAL | Gtk.DialogFlags.DESTROY_WITH_PARENT)

        self.emoji = emoji

        self.set_default_size(150, 100)

        box = self.get_content_area()
        grid = Gtk.Grid()
        grid.set_name("grid")
        grid.set_row_spacing(5)
        grid.set_column_spacing(5)
        box.add(grid)
        self.show_all()

        emoji_name = Gtk.Label(self.emoji.get("name").title())
        emoji_name.set_tooltip_text(self.emoji.get("name").title())
        emoji_name.set_name("emoji_name")
        emoji_name.set_hexpand(True)
        emoji_name.set_ellipsize(Pango.EllipsizeMode.END)
        emoji_name.set_halign(Gtk.Align.START)
        grid.attach(emoji_name, 0, 0, 1, 1)

        emoji_unicode = Gtk.Label(self.emoji.get("unicode"))
        emoji_unicode.set_name("emoji_unicode")
        emoji_unicode.set_halign(Gtk.Align.START)
        grid.attach(emoji_unicode, 0, 1, 2, 1)

        self.favorite_button = Gtk.Button()
        self.favorite_button.set_name("favorite_button")
        self.favorite_button.connect("clicked", self.on_favorite_button_clicked)
        grid.attach(self.favorite_button, 1, 0, 1, 1)

        self.copy_button = Gtk.Button("Copy")
        self.copy_button.set_name("copy_button")
        self.copy_button.connect("clicked", self.on_copy_button_clicked)
        grid.attach(self.copy_button, 2, 0, 1, 1)

        path = os.path.join(os.path.dirname(__file__), "res/emoji/png_512/" + emoji.get("unicode") + ".png")
        self.emoji_image = Gtk.Image.new_from_file(path)
        self.emoji_image.set_name("emoji_image")
        grid.attach(self.emoji_image, 0, 2, 3, 1)

        css_provider = Gtk.CssProvider()
        css_provider.load_from_path(os.path.join(os.path.dirname(__file__), "res/css/popup.css"))
        screen = Gdk.Screen.get_default()
        style_context = Gtk.StyleContext()
        style_context.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    def on_copy_button_clicked(self, event):
        clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
        clipboard.set_text(self.emoji.get("characters"), -1)

    def on_favorite_button_clicked(self, event):
        if self.favorite_button.get_style_context().has_class("active"):
            self.favorite_button.get_style_context().remove_class("active")
            EmojiKeyboard.favorites_page.remove_emoji(self.emoji)
        else:
            self.favorite_button.get_style_context().add_class("active")
            EmojiKeyboard.favorites_page.add_emoji(self.emoji)

if __name__ == "__main__":
    win = EmojiKeyboard()
    win.set_wmclass("Emoji Keyboard", "EmojiKeyboard")
    win.connect("delete-event", lambda w, e: win.hide() or True)
    win.show_all()

    signal.signal(signal.SIGINT, signal.SIG_DFL)

    tray_icon = TrayIcon(win)

    Gtk.main()
