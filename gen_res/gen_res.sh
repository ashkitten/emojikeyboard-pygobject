#!/bin/bash

rm -rf "${BASH_SOURCE%/*}/../res/"
mkdir -p "${BASH_SOURCE%/*}/../res/emoji/"
python3 "${BASH_SOURCE%/*}/gen_json.py"
cp -r "${BASH_SOURCE%/*}/emoji-data/emojione/assets/png/" "${BASH_SOURCE%/*}/../res/emoji/png_64/"
cp -r "${BASH_SOURCE%/*}/emoji-data/emojione/assets/png_512x512/" "${BASH_SOURCE%/*}/../res/emoji/png_512/"
cp -r "${BASH_SOURCE%/*}/images/" "${BASH_SOURCE%/*}/../res/images/"
cp -r "${BASH_SOURCE%/*}/css/" "${BASH_SOURCE%/*}/../res/css/"
cp "${BASH_SOURCE%/*}/emoji-data/emojione/assets/png/1f4a9.png" "${BASH_SOURCE%/*}/../res/icon.png"
