from collections import OrderedDict
import json, os, itertools

with open(os.path.join(os.path.dirname(__file__), "emoji-data/emojione/emoji.json")) as f:
    emojione_json = json.load(f, object_pairs_hook=OrderedDict)
with open(os.path.join(os.path.dirname(__file__), "emoji-data/emoji.json")) as f:
    emojidata_json = json.load(f)

tone_modifiers = [
    "1f3fb",
    "1f3fc",
    "1f3fd",
    "1f3fe",
    "1f3ff"
]

emoji_dict = OrderedDict((
    category.title(), OrderedDict((
        emojione["unicode"], {
            "name": emojione["name"],
            "keywords": emojione["keywords"],
            "unicode": emojione["unicode"],
            "characters": "\u200D".join([chr(int(codepoint, 16)) for codepoint in emojione["unicode"].split("-")]),
            "category": emojione["category"].title(),
            "hasToneModifiers": bool(next(iter([emojidata.get("skin_variations") for emojidata in emojidata_json if emojidata["unified"].lower() == emojione["unicode"].lower()]), None))
        }) for key, emojione in emojione_json.items() if emojione["category"] == category and emojione["unicode"].split("-")[-1].lower() not in tone_modifiers
    )) for category in [key for key, group in itertools.groupby([emojione["category"] for key, emojione in emojione_json.items() if emojione["category"] != "modifier"])]
)

with open(os.path.join(os.path.dirname(__file__), "../res/emoji.json"), "w") as f:
    json.dump(emoji_dict, f, indent=4)
